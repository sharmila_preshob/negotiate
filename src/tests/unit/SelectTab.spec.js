import { mount } from "@vue/test-utils";
import SelectTab from "../../components/SelectTab.vue";

describe("SelectTab.vue", () => {
  test("renders without exploding, has a div with the expected class", () => {
    const wrapper = mount(SelectTab);
    expect(wrapper.is("div")).toEqual(true);
    expect(wrapper.classes()).toContain("select-container");
  });

  test("clicking on the employer tab should call the method handler", () => {
    const wrapper = mount(SelectTab);
    wrapper.setMethods({ showEmployer: jest.fn() });
    wrapper.find(".employer-tab").trigger("click");
    expect(wrapper.vm.showEmployer).toBeCalled();
  });

  test("clicking on the employee tab should call the method handler", () => {
    const wrapper = mount(SelectTab);
    wrapper.setMethods({ showEmployee: jest.fn() });
    wrapper.find(".employee-tab").trigger("click");
    expect(wrapper.vm.showEmployee).toBeCalled();
  });
});
