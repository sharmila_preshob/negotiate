import { mount } from "@vue/test-utils";
import TabContainer from "../../components/TabContainer.vue";

describe("TabContainer.vue", () => {
  test("renders without exploding", () => {
    const wrapper = mount(TabContainer);
    expect(wrapper.is("div")).toEqual(true);
    expect(wrapper.classes()).toContain("tab-container");
  });
});
