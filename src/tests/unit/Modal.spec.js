import { mount, createLocalVue } from "@vue/test-utils";
import Modal from "../../components/Modal.vue";

describe("Modal.vue", () => {
  test("renders without exploding", () => {
    const wrapper = mount(Modal);
    expect(wrapper.is("div")).toEqual(true);
    expect(wrapper.classes()).toContain("modal");
  });

  test("clicking on the close button should call the method handler", () => {
    const wrapper = mount(Modal);
    wrapper.setMethods({ closeModal: jest.fn() });
    wrapper.find(".close").trigger("click");
    expect(wrapper.vm.closeModal).toBeCalled();
  });

  test("success result is calculated correctly", () => {
    const localVue = createLocalVue();

    let mixin = {
      mounted() {
        this.employer = 400;
        this.employee = 200;
      }
    };

    localVue.mixin(mixin);

    const wrapper = mount(Modal, {
      localVue
    });

    expect(wrapper.vm.result).toBe("Success");
  });

  test("failure result is calculated correctly", () => {
    const localVue = createLocalVue();

    let mixin = {
      mounted() {
        this.employer = 200;
        this.employee = 400;
      }
    };

    localVue.mixin(mixin);

    const wrapper = mount(Modal, {
      localVue
    });

    expect(wrapper.vm.result).toBe("Failure");
  });
});
